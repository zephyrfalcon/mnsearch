#!/bin/sh
# Create/update zip files of images. Each directory is split in parts so we
# stay under GAE's 10 Mb file size limit.

echo Cruncha cruncha cruncha...

for s in AW BS DE VS ND PR TR
do
  zip -u0 "${s}1" $s/[A-H]*
  zip -u0 "${s}2" $s/[I-P]*
  zip -u0 "${s}3" $s/[Q-Z]*
done

echo OK
