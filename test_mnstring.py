# test_mnstring.py

import unittest
import card

class TestMNString(unittest.TestCase):
    
    def test_mnstring(self):
        a = card.MNString("Weave")
        b = card.MNString("d'Resh")
        assert "weave" > "d'resh"
        assert a > b # they're case insensitive
        
    def test_search(self):
        a = card.MNString("O'Qua")
        b = card.MNString("Evu")
        assert a.search("'")
        assert b.search(".{3}")
        assert not b.search("'")
        assert a.search("O")
        assert a.search("o")
