# my_filters.py

import logging
import os
#
from google.appengine.ext import webapp
#
import card as cardmod

register = webapp.template.create_template_register()

@register.filter
def foobar(value):
    return "(%s)" % str(value)

@register.filter
def cardname_with_link(card):
    path = cardmod.cardname_to_image(card)
    #logging.info(path)
    return '<a href="/%s">%s</a>' % (path, card.name)
 
@register.filter
def released(card):
    return "(unreleased)" if card.set_long.startswith("Trai") else ""

