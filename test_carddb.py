# test_carddb.py

import unittest
#
import card
import carddb

class TestCardDB(unittest.TestCase):
    
    def setUp(self):
        # avoid loading the database for every test method =/
        carddb._cdb = getattr(carddb, '_cdb', None) or carddb.CardDB()
        self._cdb = carddb._cdb
        
    def test_mnstrings(self):
        """ Test if all strings have been replaced by MNStrings. """
        
        # pick specific cards (e.g. with dual regions, titles, etc)
        # test the attributes of these cards
        
        q = self._cdb.query(lambda c: c.regions == ["Weave", "d'Resh"])[0]
        assert isinstance(q.name, card.MNString)
        assert isinstance(q.region, card.MNString)
        assert isinstance(q.regions[0], card.MNString)
        assert isinstance(q.regions[1], card.MNString)
        assert q.regions[0] > q.regions[1]