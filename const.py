# const.py

from tools.twowaydict import TwoWayDict

VERSION = "0.10"
# 0.1: first version with a few cards
# 0.2: base set complete & API improvements
# 0.3: Awakening complete & API improvements
# 0.4: Dream's End complete &c
# 0.5: Nightmare's Dawn complete &c
# 0.6: Voice of the Storms complete &c
# 0.7: Promos complete &c
# 0.8: Improved API, documentation
# 0.9: Added Traitor's Reach
# 0.10: s/title/subtype/g

sets = TwoWayDict([('BS', 'Base Set'),
                   ('AW', 'Awakening'),
                   ('DE', "Dream's End"),
                   ('ND', "Nightmare's Dawn"),
                   ('VS', 'Voice of the Storms'),
                   ('PR', 'Promos'),
                   ('TR', 'Traitor\'s Reach'),
                  ])
                   
regions = TwoWayDict([
            ('AR', "Arderial"),
            ('CA', "Cald"),
            ('NM', "Naroom"),
            ('OR', "Orothe"),
            ('UD', 'Underneath'),
            ('UV', 'Universal'),
            ('CO', 'Core'),
            ('KT', "Kybar's Teeth"),
            ('WV', "Weave"),
            ("BG", "Bograth"),
            ("PA", "Paradwyn"),
            ('DR', "d'Resh"),
            ("NR", "Nar"),
          ])
          
rarities = TwoWayDict([
            ('C', 'Common'),
            ('U', 'Uncommon'),
            ('R', 'Rare'),
            ('P', 'Promo'),
           ])
           
types = TwoWayDict([
            ('C', 'Creature'),
            ('M', 'Magi'),
            ('S', 'Spell'),
            ('R', 'Relic')])
            
