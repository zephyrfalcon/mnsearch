# test_queries.py

import unittest
#
import carddb
import queries

T = queries.plain_text
Q = queries.python_search

class TestQueries(unittest.TestCase):
    
    def setUp(self):
        carddb._cdb = getattr(carddb, '_cdb', None) or carddb.CardDB()
        self.cdb = carddb._cdb

    def test_multiple_powers(self):
        f = Q("card.powers and len(card.powers) > 1")
        cards = self.cdb.query(f)
        assert cards, "There should be several cards with multiple powers"
        
    def test_x_cost(self):
        f = Q("str(card.cost).like('X')")
        cards = self.cdb.query(f)
        assert cards, "There could be several cards with X-cost"
        
    def test_has_power(self):
        f = Q("card.has_effect('Invulnerability')")
        cards = self.cdb.query(f)
        assert cards, "There should be cards with Effect: Invulnerability"
        
    def test_plain_search(self):
        f = T("zet")
        cards = self.cdb.query(f)
        assert cards
        
    def test_regex_search(self):
        f = Q("""card.name.search("str[aeiou]")""")
        cards = self.cdb.query(f)
        assert cards
        