# card.py

"""
Rather than having an abstract base class Card, and then subclasses like Magi,
Creature, etc, we use a generic Card class for all card types, and work from
there.
"""

import logging
import operator
import os
import re
import string
#
import const

class Null:    
    def __getattr__(self, name):
        return self
    def __getitem__(self, x):
        return self
    def __call__(self, *args, **kwargs):
        return self
    def __repr__(self):
        return MNString("")
    __str__ = __repr__
    def __nonzero__(self):
        return False
    def __len__(self):
        return 0
    def __eq__(self, other): False
    __ne__ = __eq__
    __le__ = __eq__
    __lt__ = __eq__
    __ge__ = __eq__
    __gt__ = __eq__
        
null = Null()

class Card:
    def __init__(self, d=None):
        if d is None: d = {}
        self._data = d
    
    def __getattr__(self, name):
        try:
            return self._data[name]
        except KeyError:
            return null
            
    def __getitem__(self, name):
        # NECESSARY because Django templates does a dictionary lookup first
        # when it encounters card.foo... it would get the null object back
        # rather than None or the actual attribute value.
        return self.__getattr__(name)
            
    def set_card_attr(self, name, value):
        self._data[name] = value
            
    def __repr__(self):
        return "Card(%r)" % self._data
    __str__ = __repr__
    
    # FIXME: before adding more functions, flesh out Power/Effect classes first
    def has_power(self, name):
        return self.powers and name in [p.get('name') for p in self.powers]
    def has_effect(self, name):
        return self.effects and name in [e.get('name') for e in self.effects]
        
    def has_region(self, name):
        return name in self.regions
        
    def update(self, data):
        self._data.update(data)
        
    def clone(self, updates):
        other = self.__class__(self._data.copy())
        other.update(updates)
        return other
    
#
# card name -> filename conversion
  
# note: GAE does not like "&" and "!" in filenames.
KEEP = string.ascii_letters + string.digits + " -"
REPLACE = { ' ': '_' }
def normalize_cardname(name):
    chars = [c for c in name if c in KEEP]
    name = string.join(chars, '')
    for key, value in REPLACE.items():
        name = name.replace(key, value)
    name = name.replace("__", "_")
    return name
        
def cardname_to_image(card):
    name = normalize_cardname(card.name)
    
    # add suffix for dual regions, if appropriate
    suffix = ""
    if len(card.regions) > 1:
        suffix = '_' + const.regions[card.regions[0]]
        
    path = os.path.join("images", card.set, name + suffix + ".jpg")
    return path
        
class MNString(str):
    # Basically a case-insensitive string + extra convenience methods.
    # Add methods as we go.
    def like(self, s):
        return s.lower() in self.lower()
    def search(self, s):
        # should work on both lowercase and normal strings
        return bool(re.search(s, self.lower()) or re.search(s, self))
    def startswith(self, s):
        return self.lower().startswith(s.lower())
    def endswith(self, s):
        return self.lower().endswith(s.lower())
    __contains__ = like

def register_operator(name):
    op = getattr(operator, name)
    def f(self, other, op=op):
        return op(self.lower(), other.lower())
    setattr(MNString, '__'+name+'__', f)
    
for name in ["eq", "lt", "gt", "le", "ne", "ge"]:
    register_operator(name)
    
class Power:
    pass
    
class Effect:
    pass
    
def convert_types(obj):
    if isinstance(obj, str):
        return MNString(obj)
    elif isinstance(obj, list):
        return map(convert_types, obj)
    elif isinstance(obj, dict):
        return dict((k, convert_types(v)) for (k, v) in obj.items())
    elif isinstance(obj, tuple):
        return tuple(map(convert_types, obj))
    elif isinstance(obj, Card):
        obj._data = convert_types(obj._data)
        return obj
    else:
        return obj
            
