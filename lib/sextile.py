# sextile.py

# Should we disallow backslashes, style chars, etc, in <pre> and <nomagic> 
# blocks?
# Pro: Yes, <pre> blocks are meant to display things as-is.
# Con: What to do if we actually want to use them in a <pre> block? Not 
# unlikely; < and > still need to be escaped, etc. 

# Currently, lists and headers are not expanded in pre blocks.

import re
import string
import sys

__version__ = "0.6"

class SextileError(Exception): pass

TOKEN = "\x01"      # placeholder for consecutive \ns
TOKEN2 = "\x02"     # placeholder for \ns in <pre> blocks
TOKEN4 = "\x04"     # 

CODE_BEGIN, CODE_END = "code:", ":code"

re_multiple_lineseps = re.compile("\n\n+", re.DOTALL|re.MULTILINE)
re_multiple_tokens = re.compile("\x02\x02+", re.DOTALL|re.MULTILINE)
re_pre_block = re.compile("<pre>.*?</pre>|<nomagic>.*?</nomagic>|<#.*?#>", 
               re.DOTALL|re.MULTILINE)
re_header_newline = re.compile("(</h[1-6]>)(\n)([^\n])", re.MULTILINE|re.DOTALL)

re_table_begin = re.compile("^table( *.*? *)?:$", re.DOTALL|re.MULTILINE)
re_table_end = re.compile("^:table$")

def _fixlineseps(text):
    text = string.replace(text, "\r\n", "\n")
    text = string.replace(text, "\r", "\n")
    return text
    
_backslashes = {
    ">": "&gt;",
    "<": "&lt;",
    "&": "&amp;",
    "\\": "\\", 
    "0": "",        # \0 is, literally, nothing
    "z": TOKEN4,    # mark this so we can find it later to remove <br>s
}
    
def _expand_backslashes(text):
    idx = 0
    while 1:
        idx = string.find(text, "\\", idx)
        if idx > -1:
            expr = text[idx:idx+2]
            if len(expr) != 2:
                raise SextileError, "No trailing backslash allowed"
            try:
                char = _backslashes[expr[1]]
            except KeyError:
                raise SextileError, "Unknown escape character: %s" % (expr,)
            text = text[:idx] + char + text[idx+2:]
            idx += 1
        else:
            break
            
    return text
    
_styles = {
    "<*": "<strong>",
    "*>": "</strong>",
    "<_": "<em>",
    "_>": "</em>",
    "<~": "<s>",
    "~>": "</s>",       # not <- ->, which clash with HTML comments
    "<^": "<sup>",
    "^>": "</sup>",
    "<~": "<sub>",
    "~>": "</sub>",
    "<@": "<code>",
    "@>": "</code>",
    "<=": "<u>",
    "=>": "</u>",
}
# don't use </, <!, <% or <#

def _expand_styles(text):
    for key, value in _styles.items():
        text = string.replace(text, key, value)
    return text
    
def _expand_headers(text):
    lines = string.split(text, "\n")
    for i in range(len(lines)):
        line = lines[i]
        if len(line) >= 3 and line[0] == 'h' and line[1] in '123456' \
        and line[2] == '.':
            number = line[1]
            line = line[3:].strip()
            lines[i] = "<h%s>%s</h%s>" % (number, line, number)
            
    text = string.join(lines, "\n")
    
    # line breaks directly following a <hN> should go, *unless* its a multiple
    # line break (in which case it is a paragraph separator)
    text = re_header_newline.sub(r"\1\3", text)
    
    return text
    
def _expand_lists(text):
    text = _expand_lists_helper(text, "* ", "<ul>", "</ul>")
    text = _expand_lists_helper(text, "# ", "<ol>", "</ol>")
    return text
    
def _collapse_continuations(listlines):
    llines = []
    for line in listlines:
        if llines and llines[-1].endswith("\\"):
            llines[-1] = llines[-1][:-1] + line
        else:
            llines.append(line)
        
    return llines
    
def _expand_lists_helper(text, trigger, opening, closing):
    inlist = 0
    lines = string.split(text, "\n")
    lines = _collapse_continuations(lines)
    for i in range(len(lines)):
        line = lines[i]
        if line.startswith(trigger):
            if inlist:
                lines[i] = "<li>" + line[2:] + "</li>"
            else:
                lines[i] = opening + "<li>" + line[2:] + "</li>"
                inlist = 1
        else:
            if inlist:
                lines[i-1] += closing
                inlist = 0
            
    if inlist:
        lines[-1] += closing
    
    return string.join(lines, "\n")
    
def _expand_code(text):
    #idx = string.find(text, CODE_BEGIN)
    #if idx > -1: print "find code:", repr(text[idx-10:idx+10])
    #idx = string.find(text, CODE_END)
    #if idx > -1: print "find :code", repr(text[idx-10:idx+10])
    text = string.replace(text, "\n" + CODE_BEGIN + "\n", "\n<pre>")
    text = string.replace(text, "\n" + CODE_END + "\n", "</pre>\n")
    return text
    
def findfirst(lines, f, start=0):
    for i in range(len(lines)):
        if i < start:
            continue
        result = f(lines[i])
        if result:
            return i, result
    return -1, None
    
def _expand_tables_lines(lines):
    idx1 = 0
    for i in range(100):
        idx1, m = findfirst(lines, lambda s: re_table_begin.search(s), start=idx1)
        if idx1 > -1:
            idx2, _ = findfirst(lines, lambda s: re_table_end.search(s), start=idx1)
            if idx2 > -1:
                args = m.group(1).split()
                args = dict([a.split('=') for a in args])
                table_lines = _make_table(lines[idx1+1:idx2], args)
                blurb = string.join(table_lines, TOKEN2)
                lines[idx1:idx2+1] = [blurb] #table_lines
            else:
                raise SyntaxError, "Incomplete table definition"
        else:
            return lines
            
def _expand_tables(text):
    lines = text.split('\n')
    lines = _expand_tables_lines(lines)
    return string.join(lines, '\n')
        
def _make_table(lines, args):
    try:
        dimensions = args['size']
        x, y = dimensions.split('x')
        x = int(x)
        y = int(y)
    except KeyError:
        raise SyntaxError, "Table definition must have a valid 'size' argument"
        
    def glines():
        for line in lines:
            yield line
        while 1:
            yield ""
    linemaker = glines()
        
    newlines = []
    newlines.append("<table>")
    for row in range(y):
        newlines.append(" <tr>")
        for col in range(x):
            line = linemaker.next()
            newlines.append("  <td>" + line + "</td>")
        newlines.append(" </tr>")
    newlines.append("</table>")
    
    return newlines
    
default_options = {
    "soft_line_breaks": 0,
    # if soft_line_breaks is set, single line breaks are not converted to <br>.
}
    
def handle_options(text, options):
    lines = string.split(text, "\n")
    for i in range(len(lines)-1,-1,-1):
        line = lines[i]
        if line.startswith("\option"):
            del lines[i]
            parts = string.split(line[7:])
            attr, value = parts[0], eval(parts[1])
            options[attr] = value
    
    return string.join(lines, "\n")
    
def sextile(text, options={}):

    # convert \r\n to \n; ditto for \r
    text = _fixlineseps(text)
    
    # get/set options
    _options = default_options.copy()
    _options.update(options)
    options = _options
    text = handle_options(text, options)
    
    text = _expand_code(text)
    
    # convert \n's in <pre> to TOKEN2
    for m in re_pre_block.finditer(text):
        start = m.start()
        end = start + len(m.group())
        text = text[:start] + string.replace(text[start:end], "\n", TOKEN2) + \
               text[end:]
    # XXX I suppose we could do a similar trick with backslashes, thus creating
    # a way to avoid backslash-expanding in <pre> blocks... if we must

    # expand lists. (must happen before we put the <p>s in place)
    text = _expand_lists(text)
    
    # expand hN. headers
    text = _expand_headers(text)
    
    # expand tables
    text = _expand_tables(text)

    # convert consecutive \ns to TOKEN
    text = re_multiple_lineseps.sub(TOKEN, text)
   
    # split on the special token
    parts = string.split(text, TOKEN)
    # wrap <p></p> tags around them
    for i in range(len(parts)):
        parts[i] = "<p>" + parts[i] + "</p>" + TOKEN2
    # and glue things back together
    text = string.join(parts, "")
    
    # all line breaks that were not converted to TOKEN2 are now converted
    # to <br>
    if not options.get('soft_line_breaks', 0):
        text = string.replace(text, "\n", "<br>\n")
    # convert TOKEN2 back to \n
    text = string.replace(text, TOKEN2, "\n")
    
    # expand code: block
    #text = _expand_code(text)
    
    # expand backslashes
    text = _expand_backslashes(text)    # XXX: not in pre?
    
    # expand style markers
    text = _expand_styles(text)         # XXX: not in pre?
    
    # we don't want a <br> directly before a </p>
    text = string.replace(text, "<br>\n</p>", "</p>")
    # ...nor directly after a </li>
    text = string.replace(text, "</li><br>", "</li>")
    
    # we also don't want a <br> directly after a TOKEN4 (\z legacy)
    text = string.replace(text, TOKEN4+"<br>", "")
    # remove any TOKEN4s that are left over
    text = string.replace(text, TOKEN4, "")
    
    # add line breaks after </p> so it looks better
    text = string.replace(text, "</p>", "</p>")
    
    # remove <nomagic> tags
    text = string.replace(text, "<nomagic>", "")
    text = string.replace(text, "</nomagic>", "")
    
    return text
    
if __name__ == "__main__":

    if sys.argv[1:]:
        text = file(sys.argv[1]).read()
    else:
        print "Enter some text:"
        text = sys.stdin.read()
    result = sextile(text)
    
    print result
    if not sys.argv[1:]:
        f = open("temp.html", "w")
        f.write(result)
        f.close()
        #import webbrowser
        #webbrowser.open('temp.html')
