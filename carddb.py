# carddb.py

import logging
import os
import time
#
import yaml
#
import card
import const

DEBUG = True # may disable in production environment

VALID_FIELDNAMES = [
    "alt_play",
    "artist",
    "cost",
    "energize",
    "effects",
    "flavor",
    "name",
    "powers",
    "rarity",
    "region",
    "regions",
    "set",
    "set_long",
    "shadow_play",
    "starting",
    "starting_magi",
    "type",
]

def verify_fieldnames(d):
    for key, value in d.items():
        assert key in VALID_FIELDNAMES, \
          "Unknown fieldname: %s in %r" % (key, d)

class CardDB:
    
    def __init__(self, use_yaml=True):
        self._verified = False
        t1 = time.time()
        self.data = self._load(use_yaml)
        t2 = time.time()
        logging.info("Loading took %.1fs; using yaml? %s" % (
          (t2 - t1), use_yaml))
        
    def _load(self, use_yaml=True):
        cards = []
        
        for set_digram, set_long in const.sets.items():
            cardobjs = []
            if use_yaml:
                raw_cards = _load_yaml_file(set_digram)
            else:
                raw_cards = _load_py_file(set_digram)
            for rc in raw_cards:
                if DEBUG and not self._verified:
                    verify_fieldnames(rc)
                    self._verified = True
                    
                if rc.has_key('reference'):
                    name = rc['reference']
                    # look up original card with that name
                    original = [c for c in cards 
                                if c.name == name and c.set == set_digram][0]
                    del rc['reference']
                    c = original.clone(rc)
                else:
                    c = card.Card(rc)
                
                # set additional attributes
                c.set_card_attr('set', set_digram)
                c.set_card_attr('set_long', set_long)
                c.set_card_attr('regions', 
                  filter(None, [r.strip() for r in c.region.split(',')]))
                  
                c = card.convert_types(c)
                  
                cards.append(c)
            
        return cards
        
    def query(self, f):
        found = []
        for card in self.data:
            try:
                ok = f(card)
                if ok:
                    found.append(card)
            except SyntaxError:
                # XXX maybe raise and handle further up?
                # or at least tell the user that the query was invalid
                return []
            except:
                pass
                
        return found
        
    def __len__(self):
        """ Return the number of cards in the database. """
        return len(self.data)
        
    def __iter__(self):
        return iter(self.data)

#
#

def _load_yaml_file(set_digram):
    path = os.path.join(os.path.dirname(__file__), "data",
           set_digram + ".yaml")
    try:
        f = open(path)
    except IOError:
        logging.info("Data file not found: %s.yaml" % set_digram)
        return []
    data = f.read()
    f.close()

    raw_cards = yaml.load(data)
    return raw_cards

def _load_py_file(set_digram):
    mod = __import__("pydata." + set_digram)
    return getattr(mod, set_digram).data
            