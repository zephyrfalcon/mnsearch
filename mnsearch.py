# mnsearch.py
# Main program for the Magi-Nation search engine.

import base64
import logging
import string
import os
import urllib
#
from google.appengine.ext import webapp
from google.appengine.ext.webapp import template
from google.appengine.ext.webapp.util import run_wsgi_app
from google.appengine.ext import zipserve
#
from lib import sextile
import carddb
import const
import queries

LOAD_FROM_YAML = False

template.register_template_library('common.my_filters')

cdb = carddb.CardDB(use_yaml=LOAD_FROM_YAML)
ziph = zipserve.make_zip_handler

def encode_query(expr):
    s = base64.encodestring(expr)
    return urllib.quote(s)
    
def decode_query(blurb):
    s = urllib.unquote(blurb)
    return base64.decodestring(s)

class HelpPage(webapp.RequestHandler):
    """ URLs like /doc/foo map to /doc/foo.sx, convert it to HTML, then
        stick it in templates/doc.html. """
    def get(self, name=None):
        values = {
            'name': name,
            'const': const,
            'version': const.VERSION,
        }
        
        # load Sextile source
        path = os.path.join(os.path.dirname(__file__), "doc", name+'.sx')
        f = open(path, 'rb')
        sxdata = f.read()
        f.close()
        
        html = sextile.sextile(sxdata)
        
        values['html'] = html
        
        path = os.path.join(os.path.dirname(__file__), "templates", "doc.html")
        self.response.out.write(template.render(path, values))


class SearchPage(webapp.RequestHandler):
    """ Base class for search pages. Do not instantiate; use subclasses
        instead. """
    
    template_name = "override_me.html"
    
    def get(self):
        values = {
            'dbsize': len(cdb),
            'regions': const.regions,
            'const': const,
            'version': const.VERSION,
        }
        
        path = os.path.join(os.path.dirname(__file__), "templates",
               self.template_name)
        self.response.out.write(template.render(path, values))
        
    def post(self):
        query = self.get_query()
        f = self.get_search_function(query)
        found = cdb.query(f)

        values = {
            'cards': found,
            'query': query,
            'num_cards': len(found),
            'version': const.VERSION,
            'encoded_query': encode_query(query),
            # XXX does not work for plain text queries yet
        }

        path = os.path.join(os.path.dirname(__file__), "templates",
               "results.html")
        self.response.out.write(template.render(path, values))
        
    def get_search_function(self):
        raise NotImplementedError
        
    def get_query(self):
        raise NotImplementedError
    

class SimpleSearchPage(SearchPage):
    
    template_name = "plain_search.html"
    
    def get_query(self):
        return self.request.get('query')
    
    def get_search_function(self, query):
        return queries.plain_text(query)
        
        
class AdvancedSearchPage(SearchPage):
    
    template_name = "advanced_search.html"
    
    def get_query(self):
        """ Transform input field into Python query. """
        logging.debug(self.request.body)
        
        query = ["True"]
        
        name = self.request.get("name")
        if name.strip():
            name = name.replace('"', '') # no double quotes inside string
            s = "card.name.like(\"%s\")" % name
            query.append(s)
        
        # handle regions
        region_digrams = self.request.get_all('region')
        if region_digrams:
            # we only match the *first* region, so a search for e.g. Naroom
            # will only show the Naroom version of multiple-region-cards
            # (unless the other region was specified as well).
            parts = ["card.regions[0] == \"%s\"" % const.regions[d] 
                     for d in region_digrams]
            s = "(" + string.join(parts, " or ") + ")"
            query.append(s)
            
        set_digrams = self.request.get_all('set')
        if set_digrams:
            parts = ["card.set == \"%s\"" % s for s in set_digrams]
            s = "(" + string.join(parts, " or ") + ")"
            query.append(s)
            
        type_symbols = self.request.get_all('type')
        if type_symbols:
            parts = ["card.type == \"%s\"" % const.types[t]
                     for t in type_symbols]
            s = "(" + string.join(parts, " or ") + ")"
            query.append(s)
            
        rarities = self.request.get_all('rarity')
        if rarities:
            parts = ["card.rarity == \"%s\"" % rarity
                     for rarity in rarities]
            s = "(" + string.join(parts, " or ") + ")"
            query.append(s)
        
        s = string.join(query, " and ")
        if s.startswith("True and"):
            s = s[8:]
        return s
        
    def get_search_function(self, query):
        return queries.python_search(query)
    
    
class ExpertSearchPage(SearchPage):
    
    template_name = "expert_search.html"
    
    def get_query(self):
        s = self.request.get('query')
        # XXX escaping?
        
        # normalize query
        lines = [s.strip() for s in s.split("\n") if not s.startswith('#')]
        s = string.join(lines, " ")
        
        # XXX remove unwanted names/symbols?
        
        return s
        
    def get_search_function(self, query):
        return queries.python_search(query)
        
class QueryPage(SearchPage):
    """ /query/{encoded-expr} """
    
    def get(self, query):
        self._expr = decode_query(query)
        self.post()
        
    def get_query(self):
        return self._expr

    def get_search_function(self, query):
        return queries.python_search(query)
        
#
# application
    
application = webapp.WSGIApplication(
              [('/', SimpleSearchPage),
               ('/search', SimpleSearchPage), # post
               ('/advanced', AdvancedSearchPage),
               ('/advanced_search', AdvancedSearchPage), # post
               ('/expert', ExpertSearchPage),
               ('/expert_search', ExpertSearchPage), # post
               ('/query/(.*?)', QueryPage),
               
               ('/doc/(.*?)', HelpPage),
               
               # images are in zip files so we won't run into the 1000-file
               # limit...
               ('/images/(AW/[A-H].*)', ziph('images/AW1.zip')),
               ('/images/(AW/[I-P].*)', ziph('images/AW2.zip')),
               ('/images/(AW/[Q-Z].*)', ziph('images/AW3.zip')),
               
               ('/images/(BS/[A-H].*)', ziph('images/BS1.zip')),
               ('/images/(BS/[I-P].*)', ziph('images/BS2.zip')),
               ('/images/(BS/[Q-Z].*)', ziph('images/BS3.zip')),
               
               ('/images/(DE/[A-H].*)', ziph('images/DE1.zip')),
               ('/images/(DE/[I-P].*)', ziph('images/DE2.zip')),
               ('/images/(DE/[Q-Z].*)', ziph('images/DE3.zip')),
               
               ('/images/(ND/[A-H].*)', ziph('images/ND1.zip')),
               ('/images/(ND/[I-P].*)', ziph('images/ND2.zip')),
               ('/images/(ND/[Q-Z].*)', ziph('images/ND3.zip')),
               
               ('/images/(PR/[A-H].*)', ziph('images/PR1.zip')),
               ('/images/(PR/[I-P].*)', ziph('images/PR2.zip')),
               ('/images/(PR/[Q-Z].*)', ziph('images/PR3.zip')),
               
               ('/images/(VS/[A-H].*)', ziph('images/VS1.zip')),
               ('/images/(VS/[I-P].*)', ziph('images/VS2.zip')),
               ('/images/(VS/[Q-Z].*)', ziph('images/VS3.zip')),

               ('/images/(TR/[A-H].*)', ziph('images/TR1.zip')),
               ('/images/(TR/[I-P].*)', ziph('images/TR2.zip')),
               ('/images/(TR/[Q-Z].*)', ziph('images/TR3.zip')),
              ], debug=True)
                                     
def main():
    run_wsgi_app(application)
    
if __name__ == "__main__":
    main()
    
