# twowaydict.py

class TwoWayDict:
    """ Simple two-way dictionary.  Does not follow the usual dict interface.
    """
    
    def __init__(self, pairs):
        self._pairs = pairs
        self._d1, self._d2 = self._make_dicts()
        
    def _make_dicts(self):
        d1 = dict(self._pairs)
        d2 = dict((b, a) for a, b in self._pairs)
        return d1, d2
    
    def __getitem__(self, key):
        try:
            return self._d1[key]
        except KeyError:
            pass
            
        try:
            return self._d2[key]
        except KeyError:
            pass
            
        raise KeyError(key)
        
    def get(self, key, default=None):
        try:
            return self.__getitem__(key)
        except KeyError:
            return default
            
    def items(self):
        return self._pairs
        
    def __iter__(self):
        return iter(self._pairs)
        