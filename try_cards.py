# try_cards.py

import pprint
import yaml
#
from cards import Card

data = open("cards.yaml").read()
raw_cards = yaml.load(data)

cards = []
for d in raw_cards:
    c = Card(d)
    cards.append(c)

for card in cards:
    print "%s [%s]" % (card.name, card.region)

