#!/usr/bin/env python
# serialize.py

# diddle sys.path
import os
whereami = os.path.dirname(os.path.abspath(__file__))
import sys
sys.path.append(os.path.join(whereami, ".."))

import pprint

import carddb
import const

header = """\
# %(set_digram)s.py
# Auto-generated. Do not edit!

data = \\
"""

def write_py_file(set_digram, raw_cards):
    filename = set_digram + ".py"
    out_path = os.path.join(whereami, "..", "pydata", filename)
    
    f = open(out_path, 'wb')
    f.write(header % locals())
    pprint.pprint(raw_cards, f)
    f.write("\n")
    f.close()

for set_digram, set_long in const.sets.items():
    raw_cards = carddb._load_yaml_file(set_digram)
    print "Loaded:", set_digram
    write_py_file(set_digram, raw_cards)
    
