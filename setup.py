# setup.py

from distutils.core import setup
#
import const

setup(name='mnsearch',
      version=const.VERSION,
      author="Hans Nowak",
      author_email="zephyrfalcon@gmail.com",
      maintainer="Hans Nowak",
      url="http://projects.flowsnake.org/mnsearch",
      description="Searchable card database for Magi-Nation",
      license='BSD',
      platforms=["Unix"],
      classifiers=["Google App Engine"],
     )
     