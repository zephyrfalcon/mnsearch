# queries.py

import logging
#
import card

# TODO: make this a method of Card, then turn this into a search expr
def plain_text(text):
    """ Very crude plaintext search.  Looks for the given string in the
        card's repr. """
    def _plain_text(card):
        return text.lower() in repr(card.__dict__).lower()
    return _plain_text

#
# Python search

# only certain builtins are visible:    
ALLOWED_BUILTINS = ["True", "False", "int", "long", "float", "len"]
visible_globals = {'__builtins__': {},
                   'str': card.MNString,
                  }
for name in ALLOWED_BUILTINS:
    visible_globals[name] = __builtins__[name]
    
def python_search(expr):
    def _python_search(card):
        try:
            return eval(expr, visible_globals, locals())
        except SyntaxError:
            raise
        except:
            logging.exception("evaluation went wrong")
    return _python_search
    