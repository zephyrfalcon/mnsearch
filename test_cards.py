# test_cards.py
# Run with nosetests -sv. :-)

import os
import re
import unittest
#
import carddb
import card as cardmod
import const

CARD_TYPES = [b for a, b in const.types]
REGIONS = [b for a, b in const.regions]
RARITIES = [a for a, b in const.rarities]

BASE_SET_REGIONS = ["Arderial", "Cald", "Naroom", "Orothe", "Underneath"]

# cards with non-integer cost (e.g. X+4)
X_COST = ["Syphon Vortex", "Burrow", "Thunderquake", "Maelstrom", "Turn",
          "Sorreah's Dream", "Stone Storm", "Evu's Jellybeans",
          "Sandswirl", "Quido Swarm", "Unmake", "Nightmare Construct"]
re_x_cost = re.compile("^X(\+\d+)?$")


class TestCards(unittest.TestCase):
    
    def setUp(self):
        # avoid loading the database for every test method =/
        carddb._cdb = getattr(carddb, '_cdb', None) or carddb.CardDB()
        self.cdb = carddb._cdb
        
    def card_exists(self, name):
        stuff = self.cdb.query(lambda c: c.name == name)
        return bool(stuff)
        
    def _test_region_attr(self, card, attrname):
        rs = getattr(card, attrname, None)
        if rs is not None and rs is not cardmod.null:
            if "," in rs:
                regions = [s.strip() for s in rs.split(",")]
            else:
                regions = [rs.strip()]
            for region in regions:
                assert region in REGIONS, (attrname, card)
        
    def test_images(self):
        dirs = {}
        for setname, _ in const.sets:
            path = os.path.join(os.path.dirname(os.path.abspath(__file__)),
                   "images", setname)
            if os.path.exists(path):
                filenames = os.listdir(path)
                dirs[setname] = [os.path.splitext(fn)[0] for fn in filenames]
        
        for card in self.cdb:
            # do we have an image?
            image_path = cardmod.cardname_to_image(card)
            assert os.path.exists(image_path), image_path
            filename = os.path.split(image_path)[1]
            basename = os.path.splitext(filename)[0]
            assert basename in dirs[card.set], \
                   "Image not found: %s/%s.jpg" % (card.set, basename)
    
    def test_all_cards(self):
        for card in self.cdb:
            
            assert card.name, card
            assert card.cost is not None, card
            if card.name in X_COST:
                assert isinstance(card.cost, str), card
                assert re_x_cost.match(card.cost), card
            else:
                assert isinstance(card.cost, int), card
            assert card.type, card
            assert card.region, card
            assert card.regions, card
            assert card.rarity, card
            
            assert card.type in CARD_TYPES, card
            for region in card.regions:
                assert region in REGIONS, card # handles double regions as well
            assert card.rarity in RARITIES, card
            
            assert card.artist, card # lame!
            
            if card.type == "Magi":
                assert card.energize is not None, card # all Magis energize
                assert isinstance(card.energize, int)

                # verify that "starting" cards exist
                starting = card.starting or [] # must be a list
                for name in starting:
                    if name.startswith('[') or name.startswith('?'):
                        continue # not an actual card name; ignore
                    assert self.card_exists(name), (name, card)
                    
            if card.type == "Spell":
                assert card.text, card
                
            if card.starting_magi:
                # can be a string (one Magi) or a list
                assert card.type != "Magi"
                sm_names = []
                if isinstance(card.starting_magi, str):
                    sm_names = [card.starting_magi]
                elif isinstance(card.starting_magi, list):
                    sm_names = card.starting_magi
                else:
                    assert False, (starting_magi, card)
                    
                for name in sm_names:
                    if name.startswith('['):
                        continue # not an actual card name; ignore
                    assert self.card_exists(name), (name, card)
                    
            if card.alternate:
                if isinstance(card.alternate, list):
                    for acard in card.alternate:
                        assert self.card_exists(acard), (acard, card)
                else:
                    assert self.card_exists(card.alternate), (card.alternate, card)

            # if these attributes are present, they must be a valid region,
            # or a comma-separated list of valid regions:
            self._test_region_attr(card, 'shadow_play')
            self._test_region_attr(card, 'alt_play')
            self._test_region_attr(card, 'original_region')
            
            if card.region == 'Core':
                assert not card.shadow_play, card
            else:
                # alt_play may occur on non-Core cards (like Swamp Hyren)
                # to indicate that Core may play the card
                assert (not card.alt_play) or (card.alt_play == 'Core'), card
            
    def test_specific_cards(self):
        pass
        # test specific cards, e.g. creatures with energize, multiple powers,
        # etc.
 
    def test_numbers_base_set(self):
        types = [("Creature", 16),
                 ("Magi", 7),
                 ("Relic", 5),
                 ("Spell", 7)]
        for region in BASE_SET_REGIONS:
            for type, number in types:
                f = lambda c: (c.set == 'BS' and c.region == region 
                               and c.type == type)
                num_cards = len(self.cdb.query(f))
                assert number == num_cards, \
                  "BaseSet: Expected %d cards of type %s for %s, got %d" % (
                    number, type, region, num_cards)
                    
        z = self.cdb.query(lambda c: c.region == 'Universal' and c.set == 'BS')
        assert len(z) == 15
        
    def test_numbers_awakening(self):
        for region in BASE_SET_REGIONS:
            self._test_numbers(7, set='AW', type='Creature', region=region)
            self._test_numbers(3, set='AW', type='Magi', region=region)
            self._test_numbers(3, set='AW', type='Relic', region=region)
            self._test_numbers(3, set='AW', type='Spell', region=region)
            
        self._test_numbers(10, set='AW', region='Universal')
            
        core_cards = [
            ('Creature', 22),
            ('Magi', 10),
            ('Relic', 6),
            ('Spell', 12)]
        for type, number in core_cards:
            self._test_numbers(number, set='AW', type=type, region='Core')

        # test Shadow Magi, regular Magi
        f = lambda c: (c.set == 'AW' and c.type == 'Magi' and c.region == 'Core'
                       and c.original_region)
        z = self.cdb.query(f)
        assert len(z) == 6, "There are 6 Shadow Magi in Awakening"
        
    def test_numbers_dreams_end(self):
        # also test Core, Weave, KT
        
        distr = [('Creature', 5),
                 ('Magi', 2),
                 ('Relic', 2),
                 ('Spell', 2)]
        for region in BASE_SET_REGIONS:
            for type, number in distr:
                self._test_numbers(number, set='DE', type=type, region=region)
                
        kt_distr = [('Creature', 22),
                    ('Magi', 10),
                    ('Relic', 9),
                    ('Spell', 9)]
        for region in ["Kybar's Teeth", "Weave"]:
            for type, number in kt_distr:
                self._test_numbers(number, set='DE', type=type, 
                                   region=region)
                                   
    def test_numbers_nightmares_dawn(self):
        distr = {
            # region: (creatures, magi, relics, spells)
            "Arderial":      (9, 3, 2, 2),
            "Bograth":       (33, 15, 12, 13),
            "Cald":          (7, 3, 3, 2),
            "Core":          (8, 3, 3, 2),
            "d'Resh":        (4, 1, 1, 1),
            "Kybar's Teeth": (12, 4, 5, 3),
            "Nar":           (1, 0, 0, 1),
            "Naroom":        (9, 3, 2, 1),
            "Orothe":        (8, 3, 2, 2),
            "Paradwyn":      (35, 14, 11, 14),
            "Underneath":    (9, 3, 2, 2),
            "Universal":     (0, 1, 2, 2),
            "Weave":         (13, 4, 3, 2),
        }
        for region, numbers in distr.items():
            types = ['Creature', 'Magi', 'Relic', 'Spell']
            for t, n in zip(types, numbers):
                self._test_numbers(n, set='ND', type=t, region=region)
                
    def test_numbers_vots(self):
        distr = {
            # region: (creatures, magi, relics, spells)
            "Arderial":      (6, 2, 2, 4),
            "Bograth":       (7, 3, 2, 3),
            "Cald":          (6, 2, 3, 3),
            "Core":          (6, 4, 1, 4),
            "d'Resh":        (35, 13, 10, 17),
            "Kybar's Teeth": (8, 3, 2, 2),
            "Nar":           (37, 13, 12, 13),
            "Naroom":        (7, 3, 2, 2),
            "Orothe":        (6, 3, 3, 2),
            "Paradwyn":      (8, 3, 2, 2),
            "Underneath":    (6, 2, 2, 4),
            "Universal":     (1, 0, 1, 3),
            "Weave":         (6, 2, 4, 3),
        }
        for region, numbers in distr.items():
            types = ['Creature', 'Magi', 'Relic', 'Spell']
            for t, n in zip(types, numbers):
                self._test_numbers(n, set='VS', type=t, region=region)
                               
    def _test_numbers(self, total, set=None, type=None, region=None):
        def _f_(card):
            match = True
            if set:
                match = match and card.set == set
            if region:
                match = match and card.regions[0] == region
            if type:
                match = match and card.type == type
            return match
        z = self.cdb.query(_f_)
        assert len(z) == total, \
          "Expected %d cards for %r, got %d instead: %r" % (total, 
            {'set': set, 'type': type, 'region': region}, len(z),
            [c.name for c in z])
                    
    def test_rarity(self):
        rarities = [("Magi", [0, 4, 3]),]
        # XXX TODO: add other types as well
        
        for region in BASE_SET_REGIONS:
            for type, rar in rarities:
                for rtype, rnum in zip(RARITIES, rar):
                    f = lambda c: (c.region == region and c.type == type
                        and c.rarity == rtype and c.set == 'BS')
                    num_cards = len(self.cdb.query(f))
                    assert rnum == num_cards, \
                      "BaseSet: Expected %d cards of type %s and rarity %s for %s, got %d" % (
                        rnum, type, rtype, region, num_cards)

    def test_multiple_powers(self):
        cards = self.cdb.query(lambda c: c.powers and len(c.powers) > 1)
        assert cards
        
    def test_dual_regions(self):
        cards = self.cdb.query(lambda c: c.set == 'DE' and c.name == 'Bungaloo')
        assert len(cards) == 2, "There should be two Bungaloos in DE"
        for c in cards:
            assert c.has_region("Naroom")
            assert c.has_region("Weave")
            assert c.has_region('naroom') # case insensitive

        # dual-region cards must have two versions
        cards = self.cdb.query(lambda c: len(c.regions) > 1)
        for c in cards:
            assert len(c.regions) > 1
            assert [d for d in cards 
                    if d.name == c.name 
                    and d.regions == list(reversed(c.regions))],\
                    "missing counterpart to: %r" % c
                    
    def test_invariants(self):
        # Magis are never common:
        magis = self.cdb.query(lambda c: c.type == 'Magi')
        for magi in magis:
            assert magi.rarity != "C", \
              "Magis are never common: %r" % c
            
    # TODO:
    # test Core Magi, Shadow Magi, cards that can be played by Shadow Magi
    
