# console.py
# Simple interactive console.

import readline
import sys
#
import carddb
import const
import queries

intro = "Magi-Nation search %s command line" % const.VERSION

class Console:
    ps1 = ">> "
    ps2 = ".. "
    
    def _load(self):
        print "Loading cards..."
        self._cdb = carddb.CardDB(use_yaml=False)
        print " OK (%d cards in database)" % len(self._cdb)
        
    def query(self, expr):
        f = queries.python_search(expr)
        cards = self._cdb.query(f)
        return cards
    
    def loop(self):
        print intro
        self._load()
        while 1:
            sys.stdout.write(self.ps1)
            line = raw_input()
            # assume the line is a valid Python expression
            cards = self.query(line)
            for card in cards:
                self.display_card(card)
            print len(cards), "cards found."
            
    def display_card(self, card):
        print "[%s] %s | %s | %s | %s" % (card.set, card.name, card.region,
              card.type, card.cost)
    
if __name__ == "__main__":
    
    c = Console()
    c.loop()
    